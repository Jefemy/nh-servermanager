package main

import (
	"bytes"
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	pb "gitlab.com/brickhill/servermanager/manager/protobuf/hostservice"
)

var (
	seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))
)

type Manager struct {
	laddr            *net.TCPAddr
	listener         *net.TCPListener
	availableServers []*Server
	executor         *Executor
	serverMutex      sync.Mutex
	inShutdown       bool
}

func (m *Manager) findSubServer(netID uint64) *SubServer {
	for k := range m.availableServers {
		if subServer, ok := m.availableServers[k].subServers[netID]; ok {
			return subServer
		}
	}

	return nil
}

func (m *Manager) findSubServerByContainerID(contID string) *SubServer {
	for k := range m.availableServers {
		for _, subServer := range m.availableServers[k].subServers {
			if subServer.container.ID == contID {
				return subServer
			}
		}
	}

	return nil
}

func (m *Manager) findSubServerServer(netID uint64) *Server {
	for k := range m.availableServers {
		for _, subServer := range m.availableServers[k].subServers {
			if subServer.container.netID == netID {
				return m.availableServers[k]
			}
		}
	}

	return nil
}

func (m *Manager) findServerBySetID(setID uint64) *Server {
	for k, server := range m.availableServers {
		if server.setID == setID {
			return m.availableServers[k]
		}
	}

	return nil
}

func (m *Manager) cleanServers() {
	for {
		<-time.After(15 * time.Second)
		for i, server := range m.availableServers {
			for k, subServer := range server.subServers {
				running, err := m.executor.IsContainerRunning(subServer.container.ID)
				if err != nil {
					log.Println("error determining if container running", err)
					continue
				}

				// TODO: if not running, that means that the server likely crashed, send data to bh endpoint to find errors

				if !running && !server.inShutdown {
					logs, err := subServer.GetLogs("30")
					if err != nil && !strings.Contains(err.Error(), "No such container") {
						log.Println("error getting container logs", err)
						continue
					}
					if len(logs) > 0 {
						type postCrashReport struct {
							HostKey string `json:"host_key"`
							Report  string `json:"report"`
						}
						values := postCrashReport{server.hostKey, logs}
						json_data, _ := json.Marshal(values)

						go http.Post("https://api.brick-hill.com/v1/games/postServerCrash", "application/json", bytes.NewBuffer(json_data))
					}
				}

				if (!running || len(subServer.proxies) == 0) && !subServer.inStartup {
					delete(server.subServers, k)
				}

				// clear queue of closed proxies
				// TODO: should this be a channel? there is already an erred signal but i dont think you are supposed to listen to the same channel in multiple places
				// TODO: maybe find a way to restructure code to have proxy be cleared where it is already being listened at
				for i := len(subServer.proxies) - 1; i >= 0; i-- {
					proxy := subServer.proxies[i]
					if proxy.erred {
						subServer.mutex.Lock()
						subServer.proxies = removeProxy(subServer.proxies, i)
						subServer.mutex.Unlock()
					}
				}
			}

			// clean old notifications to prevent storing them all
			notiCount := len(server.notifications)
			for k, noti := range server.notifications {
				if time.Now().Unix()-noti.sentAt > 30 {
					k = k - (notiCount - len(server.notifications))

					m.availableServers[i].notifications = append(m.availableServers[i].notifications[:k], m.availableServers[i].notifications[k+1:]...)
				}
			}
		}
	}
}

func (m *Manager) cleanContainers() {
	for {
		<-time.After(15 * time.Second)
		containers, err := m.executor.ListManagedContainers()
		if err == nil {
			for _, cont := range containers {
				ss := m.findSubServerByContainerID(cont.ID)
				if ss == nil {
					go func(contID string) {
						err := m.executor.ContainerRemove(contID)
						if err != nil {
							log.Println(err)
						}
					}(cont.ID)
				}
			}
		} else {
			log.Println("error listing managed containers", err)
		}
	}

}

func (m *Manager) watchNotifications() {
	for {
		func() {
			<-time.After(time.Second * 10)
			client, conn, err := connectToHostService()
			if err != nil {
				log.Println("gRPC connection error", err)
				return
			}
			defer conn.Close()

			var data []*pb.RetrieveServerDataRequest

			for _, server := range m.availableServers {
				data = append(data, &pb.RetrieveServerDataRequest{
					SetId:   server.setID,
					HostKey: server.hostKey,
				})
			}

			notifs, err := client.RetrieveNotifications(context.Background(), &pb.RetrieveNotificationsRequest{ServerData: data})
			if err != nil {
				log.Println("gRPC message error", err)
				return
			}

			for _, noti := range notifs.GetNotification() {
				server := m.findServerBySetID(noti.GetSetId())
				if server == nil {
					return
				}

				switch noti.MessageType {
				// Shutdown request
				case 1:
					server.notifications = append(server.notifications, ServerNotification{
						MsgType: 1,
						sentAt:  time.Now().Unix(),
					})
					server.inShutdown = true
				// New max player count
				case 2:
					type maxPlayers struct {
						MaxPlayers int `json:"max_players"`
					}
					var jsonData maxPlayers

					err = json.Unmarshal([]byte(noti.GetBody()), &jsonData)
					if err != nil {
						return
					}
					server.maxPlayers = jsonData.MaxPlayers
				// New set version
				case 3:
					// TODO: error checking???
					server.retrieveFiles()
				}
			}
		}()
	}
}

func (m *Manager) registerManager() {
	type registerManager struct {
		Secret string `json:"secret"`
	}
	values := registerManager{os.Getenv("IS_OFFICIAL")}
	json_data, _ := json.Marshal(values)

	http.Post("https://api.brick-hill.com/v1/games/registerManager", "application/json", bytes.NewBuffer(json_data))
}

func (m *Manager) postServers() {
	originalCount := len(m.availableServers)
	m.serverMutex.Lock()
	for i, server := range m.availableServers {
		var allPlayers []string
		for _, subServer := range server.subServers {
			for _, v := range subServer.players {
				allPlayers = append(allPlayers, v)
			}
		}

		type postServer struct {
			HostKey string   `json:"host_key"`
			Port    int      `json:"port"`
			Players []string `json:"players"`
		}

		values := postServer{server.hostKey, int(hostPort), allPlayers}
		json_data, _ := json.Marshal(values)

		// host keys can potentially change - as simple prevention just delete the value so the manager can redetermine if its allowed to be hosted still
		// code moved here to prevent issue where sets would get stuck with 1 player due to not being posted with 0 players ever
		if os.Getenv("INIT_ALL_SERVERS") != "true" {
			if len(server.subServers) == 0 {
				i = i - (originalCount - len(m.availableServers))

				m.availableServers = append(m.availableServers[:i], m.availableServers[i+1:]...)
			}
		}

		go http.Post("https://api.brick-hill.com/v1/games/postServer", "application/json", bytes.NewBuffer(json_data))
	}
	m.serverMutex.Unlock()
}

func (m *Manager) Listen() {
	for {
		// TODO: this function is massive, need to split it into more managable parts
		conn, err := m.listener.AcceptTCP()
		if err != nil {
			log.Println("failed to accept connection", err)
			// wait a few seconds before continuing
			// not sure if this is necessary or if it would never be in a situation where AcceptTCP can recover
			<-time.After(5 * time.Second)
			continue
		}

		go func() {
			buff := make([]byte, 0xfff)
			num, _ := conn.Read(buff)
			buff = buff[:num]
			token, err := getAuthToken(buff)
			if err != nil {
				conn.Close()
				return
			}

			client, grpcConn, err := connectToHostService()
			if err != nil {
				log.Println("gRPC connection error", err)
				conn.Close()
				return
			}
			defer grpcConn.Close()
			joining, err := client.PlayerAttemptingToJoin(context.Background(), &pb.PlayerAttemptingToJoinRequest{AuthToken: token})
			if err != nil {
				log.Println("gRPC message error", err, token, joining.GetSetId(), joining.GetPlayerId())
				conn.Close()
				return
			}
			setID := joining.GetSetId()

			// users who are still in queue during shutdown event will continue to try to join
			// just close their connection
			// TODO: manager needs to learn how to send certain client packets, like kicks to send users proper messages for why they cant join
			if m.inShutdown {
				conn.Close()
				return
			}

			server := m.findServerBySetID(setID)
			if server == nil {
				canHost, err := client.ServerCanBeHosted(context.Background(), &pb.ServerCanBeHostedRequest{SetId: setID})
				if err != nil {
					log.Println("gRPC message error", err, token, joining.GetSetId(), joining.GetPlayerId())
					conn.Close()
					return
				}
				if canHost.GetCan() {
					server = &Server{
						setID:      setID,
						hostKey:    canHost.GetHostKey(),
						subServers: make(map[uint64]*SubServer),
						maxPlayers: int(canHost.GetMaxPlayers()),
						inShutdown: false,
					}

					err = server.retrieveFiles()
					if err != nil {
						log.Println("error retrieving files", err)
						conn.Close()
						return
					}

					m.serverMutex.Lock()
					m.availableServers = append(m.availableServers, server)
					m.serverMutex.Unlock()
				} else {
					log.Println("Client tried to connect to set not being hosted")
					conn.Close()
					return
				}
			}

			server.joinMutex.Lock()
			defer server.joinMutex.Unlock()

			// dont accept connections while attempting to shutdown so everything is properly cleared
			if server.inShutdown {
				conn.Close()
				return
			}

			// run check through all servers to make sure user isnt already playing
			for _, subServer := range server.subServers {
				if _, ok := subServer.players[joining.GetPlayerId()]; ok {
					conn.Close()
					return
				}
			}

			for _, subServer := range server.subServers {
				if len(subServer.proxies) < subServer.maxPlayerCount {
					time.Sleep(500 * time.Millisecond)
					go subServer.Connect(conn, m.laddr, buff, joining.GetPlayerId())
					return
				}
			}

			/*
				docker network create --driver=bridge --subnet=172.20.0.0/16 no-internet
				sudo iptables --insert DOCKER-USER -s 172.20.0.0/16 -j REJECT --reject-with icmp-port-unreachable
				sudo iptables --insert DOCKER-USER -s 172.20.0.0/16 -m state --state RELATED,ESTABLISHED -j RETURN
			*/
			validatorBytes := make([]byte, 64)
			seededRand.Read(validatorBytes)

			validator := hex.EncodeToString(validatorBytes)

			resp, err := m.executor.ContainerStart([]string{
				fmt.Sprintf("NH_HOST_KEY=%s", "thisisgarbagetofakenodehillintothinkingitsarealhostkey1234567890"),
				fmt.Sprintf("NH_VALIDATOR=%s", validator),
				fmt.Sprintf("NH_SET=%d", server.setID),
				fmt.Sprintf("NH_LOCAL=%s", os.Getenv("LAUNCH_AS_LOCAL")),
				fmt.Sprintf("NET_ID=%d", netid),
			}, server.bundlePath)
			if err != nil {
				// TODO:`Error response from daemon: Error processing tar file(exit status 1): unexpected EOF` is extremely common here -- look into how this is possible
				log.Println("error starting container", err)
				conn.Close()
				return
			}

			port := resp.NetworkSettings.Ports["42480/tcp"][0].HostPort

			addr, _ := net.ResolveTCPAddr("tcp", fmt.Sprintf("localhost:%s", port))
			subServer := &SubServer{
				addr:        addr,
				startedChan: make(chan bool),
				players:     make(map[uint64]string),
				container: Container{
					resp.ID,
					netid,
					validator,
					LogHandler{LastTimestamp: strconv.FormatInt(time.Now().Unix(), 10)},
				},
				maxPlayerCount:   server.maxPlayers,
				inStartup:        true,
				startupTimestamp: time.Now().Unix(),
			}
			server.subServers[netid] = subServer

			netid++

			// timeout after 5 seconds to prevent deadlock
			select {
			case <-subServer.startedChan:
				// closing channels might not be necessary?
				close(subServer.startedChan)
				subServer.inStartup = false
				go subServer.Connect(conn, m.laddr, buff, joining.GetPlayerId())
			case <-time.After(30 * time.Second):
				subServer.inStartup = false
				conn.Close()
				return
			}
		}()
	}
}
