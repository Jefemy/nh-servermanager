package main

import (
	"io"
	"log"
	"net"
	"sync"
	"time"
)

type UniqueCounter struct {
	mutex sync.Mutex
	value uint64
}

func (c *UniqueCounter) NextValue() uint64 {
	c.mutex.Lock()
	c.value++
	c.mutex.Unlock()

	return c.value
}

type Proxy struct {
	laddr, raddr *net.TCPAddr
	lconn, rconn io.ReadWriteCloser
	//remoteIP       string
	uniqueId       uint64
	userId         uint64
	erred          bool
	errsig         chan bool
	sentBytes      uint64
	receivedBytes  uint64
	bytesTimestamp int64
}

var (
	counter = UniqueCounter{value: 0}
)

func New(lconn *net.TCPConn, laddr, raddr *net.TCPAddr, userId uint64) *Proxy {
	return &Proxy{
		lconn:    lconn,
		laddr:    laddr,
		raddr:    raddr,
		uniqueId: counter.NextValue(),
		userId:   userId,
		// TODO: ipv6 safe?
		//remoteIP:       strings.Split(lconn.RemoteAddr().String(), ":")[0],
		erred:          false,
		errsig:         make(chan bool),
		bytesTimestamp: time.Now().Unix(),
	}
}

type setNoDelayer interface {
	SetNoDelay(bool) error
}

func removeProxy(s []*Proxy, i int) []*Proxy {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}

// Start - open connection to remote and start proxying data.
func (p *Proxy) Start(b []byte) {
	var err error
	// connect to remote
	p.rconn, err = net.DialTCP("tcp", nil, p.raddr)
	if err != nil {
		log.Printf("Remote connection failed: %s", err)
		return
	}

	if conn, ok := p.lconn.(setNoDelayer); ok {
		conn.SetNoDelay(true)
	}
	if conn, ok := p.rconn.(setNoDelayer); ok {
		conn.SetNoDelay(true)
	}

	p.rconn.Write(b)

	// bidirectional copy
	go p.pipe(p.lconn, p.rconn)
	go p.pipe(p.rconn, p.lconn)
}

func (p *Proxy) err(s string, err error) {
	if p.erred {
		return
	}
	//if err != io.EOF {
	// half of the logs are due to this
	// is something handling an error wrong or is it just a feature
	// log.Println(s, err)
	//}
	p.errsig <- true
	p.erred = true
}

func (p *Proxy) pipe(src, dst io.ReadWriter) {
	islocal := src == p.lconn

	// directional copy (4k buffer)
	buff := make([]byte, 0xfff)
	for {
		n, err := src.Read(buff)
		if err != nil {
			p.err("Read failed\n", err)
			return
		}
		b := buff[:n]

		// write out result
		n, err = dst.Write(b)
		if err != nil {
			p.err("Write failed\n", err)
			return
		}

		if islocal {
			p.sentBytes += uint64(n)
		} else {
			p.receivedBytes += uint64(n)
		}

		if time.Now().Unix()-p.bytesTimestamp > 60 {
			p.bytesTimestamp = time.Now().Unix()
			p.sentBytes = 0
			p.receivedBytes = 0
		}

		// TODO: read more into how much data nh should be using
		if p.sentBytes > (1024 * 512) {
			p.err("Proxy sent over 512KB in 60 seconds\n", err)
			return
		}
	}
}

func (p *Proxy) closeConnection() {
	defer p.lconn.Close()
	defer p.rconn.Close()

	<-p.errsig
}
