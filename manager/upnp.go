package main

import (
	"fmt"
	"log"

	"gitlab.com/NebulousLabs/go-upnp"
)

func forwardPort(port uint16) {
	// connect to router
	d, err := upnp.Discover()
	if err != nil {
		log.Fatal(err)
	}

	// discover external IP
	ip, err := d.ExternalIP()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Your external IP is:", ip)

	// forward a port
	err = d.Forward(port, "upnp test")
	if err != nil {
		log.Fatal(err)
	}
}
