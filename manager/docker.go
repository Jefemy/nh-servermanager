package main

import (
	"bytes"
	"context"
	"log"
	"os"
	"strings"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/api/types/strslice"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/stdcopy"
	"github.com/docker/go-connections/nat"
)

var namespace = "com.brick-hill.hoster"

type Executor struct {
	client  *client.Client
	context context.Context
}

func NewExecutor() *Executor {
	client, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	_, err = client.ImagePull(ctx, "registry.gitlab.com/jefemy/nh-servermanager/node-hill:latest", types.ImagePullOptions{})
	if err != nil {
		panic(err)
	}

	return &Executor{
		client,
		ctx,
	}
}

func (e *Executor) ListManagedContainers() (containers []types.Container, err error) {
	containers, err = e.client.ContainerList(e.context, types.ContainerListOptions{
		All: true,
		Filters: filters.NewArgs(filters.KeyValuePair{
			Key:   "label",
			Value: namespace + ".managed=true",
		}),
	})

	return
}

func (e *Executor) RemoveManagedContainers() {
	containers, err := e.ListManagedContainers()
	if err != nil {
		log.Println(err)
	}
	for _, container := range containers {
		go func(container types.Container) {
			if container.State == "running" {
				timeout := 30 * time.Second
				err := e.client.ContainerStop(e.context, container.ID, &timeout)
				if err != nil {
					log.Println(err)
				}
			}

			err := e.client.ContainerRemove(e.context, container.ID, types.ContainerRemoveOptions{})
			if err != nil {
				log.Println(err)
			}
		}(container)
	}
}

func (e *Executor) ContainerRemove(contID string) (err error) {
	timeout := 30 * time.Second
	err = e.client.ContainerStop(e.context, contID, &timeout)
	if err != nil {
		return err
	}

	err = e.client.ContainerRemove(e.context, contID, types.ContainerRemoveOptions{})

	return err
}

func (e *Executor) ContainerStart(env []string, bundlePath string) (fullContainer types.ContainerJSON, err error) {
	trueBool := true

	resp, err := e.client.ContainerCreate(e.context, &container.Config{
		Image: "registry.gitlab.com/jefemy/nh-servermanager/node-hill:latest",
		Tty:   false,
		Env:   env,
		ExposedPorts: nat.PortSet{
			nat.Port("42480/tcp"): struct{}{},
		},
		Labels: map[string]string{
			namespace + ".managed": "true",
		},
	}, &container.HostConfig{
		SecurityOpt: []string{"no-new-privileges:true"},
		CapDrop:     strslice.StrSlice{"all"},
		PortBindings: nat.PortMap{
			nat.Port("42480/tcp"): []nat.PortBinding{
				{
					HostIP:   "0.0.0.0",
					HostPort: "",
				},
			},
		},
		Init: &trueBool,
		Resources: container.Resources{
			// TODO: increased cpu limit for special trusted users
			NanoCPUs: 500000000,
			Memory:   1024 * 1024 * 128, // 128 MB
		},
		//Runtime: "runsc",
	}, &network.NetworkingConfig{
		EndpointsConfig: map[string]*network.EndpointSettings{
			"no-internet": {},
		},
	}, nil, "")
	if err != nil {
		return
	}

	if len(bundlePath) > 0 {
		err = e.CopyToContainer(resp.ID, "/home/nhuser/nodehill", bundlePath)
		if err != nil {
			return
		}
	}

	if err = e.client.ContainerStart(e.context, resp.ID, types.ContainerStartOptions{}); err != nil {
		return
	}

	if fullContainer, err = e.client.ContainerInspect(e.context, resp.ID); err != nil {
		return
	}

	return
}

func (e *Executor) CopyToContainer(contID, path, localFile string) error {
	f, err := os.Open(localFile)
	if err != nil {
		return err
	}
	err = e.client.CopyToContainer(e.context, contID, path, f, types.CopyToContainerOptions{})
	if err != nil {
		return err
	}

	return nil
}

func (e *Executor) IsContainerRunning(contID string) (bool, error) {
	data, err := e.client.ContainerInspect(e.context, contID)
	if err != nil {
		if strings.Contains(err.Error(), "No such container") {
			return false, nil
		}

		return false, err
	}

	return data.State.Running, nil
}

func (e *Executor) ContainerUpdateCPU(contID string, cpuPercent int) error {
	_, err := e.client.ContainerUpdate(e.context, contID, container.UpdateConfig{
		Resources: container.Resources{
			NanoCPUs: int64(cpuPercent) * 1e7,
			Memory:   1024 * 1024 * 128,
		},
	})

	return err
}

func (e *Executor) ContainerLogs(contID string, timestamp string, tail string) (string, error) {
	var containerBuffer bytes.Buffer

	options := types.ContainerLogsOptions{
		ShowStdout: true,
		ShowStderr: true,
		Since:      timestamp,
		Tail:       tail,
	}

	hijacked, err := e.client.ContainerLogs(e.context, contID, options)
	if err != nil {
		return "", err
	}
	defer func() { hijacked.Close() }()

	stdcopy.StdCopy(&containerBuffer, &containerBuffer, hijacked)
	containerLog := containerBuffer.String()
	return strings.TrimSpace(containerLog), nil
}
