package main

import (
	"errors"
	"fmt"
	"net"
	"os"

	"gitlab.com/brickhill/servermanager/manager/protobuf/hostservice"
	"google.golang.org/grpc"
)

func connectToHostService() (c hostservice.HostServiceClient, conn *grpc.ClientConn, err error) {
	if os.Getenv("USE_SRV") == "true" {
		_, srvs, _ := net.LookupSRV("", "", os.Getenv("HOSTSERVICE_IP"))
		if len(srvs) == 0 {
			return nil, nil, errors.New("no HostService is available")
		}
		target := srvs[0]
		conn, err = grpc.Dial(fmt.Sprintf("%s:%d", target.Target, target.Port), grpc.WithInsecure())
	} else {
		conn, err = grpc.Dial(fmt.Sprintf("%s:%s", os.Getenv("HOSTSERVICE_IP"), os.Getenv("HOSTSERVICE_PORT")), grpc.WithInsecure())
	}
	if err != nil {
		return nil, nil, err
	}

	c = hostservice.NewHostServiceClient(conn)

	return
}
