package main

import (
	"context"
	"fmt"
	"net"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/brickhill/servermanager/manager/protobuf/hostservice"
)

type SubServer struct {
	addr             *net.TCPAddr
	startedChan      chan bool
	mutex            sync.Mutex
	container        Container
	players          map[uint64]string
	proxies          []*Proxy
	maxPlayerCount   int
	inStartup        bool
	startupTimestamp int64
}

type Container struct {
	ID        string
	netID     uint64
	validator string
	Logs      LogHandler
}

type LogHandler struct {
	Logs          string
	LastTimestamp string
}

type Server struct {
	setID         uint64
	hostKey       string
	maxPlayers    int
	subServers    map[uint64]*SubServer
	bundlePath    string
	inShutdown    bool
	joinMutex     sync.Mutex
	notifications []ServerNotification
}

type ServerNotification struct {
	sentAt  int64
	readBy  []uint64
	MsgType int
	Body    string
}

func (s *Server) retrieveFiles() error {
	client, conn, err := connectToHostService()
	if err != nil {
		return err
	}
	defer conn.Close()
	data, err := client.RetrieveServerData(context.Background(), &hostservice.RetrieveServerDataRequest{SetId: s.setID, HostKey: s.hostKey})
	if err != nil {
		return err
	}

	// TODO: why did i assume all external files are already tars?
	if os.Getenv("FILES_HOSTED_EXTERNALLY") == "true" {
		if len(data.GetBundleUrl()) > 0 {
			s.bundlePath = filepath.Join(tmpDir, fmt.Sprintf("%dset.tar", s.setID))
			err = saveFileFromUrl(data.GetBundleUrl(), s.bundlePath)
			if err != nil {
				return err
			}
		}
	} else {
		// TODO: in this situation it will fail to send the proper files to nh if the bundleurl is not named set.bbrk
		isSetTar := strings.HasPrefix(filepath.Ext(data.GetBundleUrl()), ".tar")
		if isSetTar {
			s.bundlePath = data.GetBundleUrl()
		} else if len(data.GetBundleUrl()) > 0 {
			s.bundlePath = filepath.Join(tmpDir, fmt.Sprintf("%dset.tar", s.setID))
			out, err := os.Create(s.bundlePath)
			if err != nil {
				return err
			}
			defer out.Close()

			err = createArchive([]string{data.GetBundleUrl()}, out)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (s *SubServer) Connect(conn *net.TCPConn, laddr *net.TCPAddr, firstPacket []byte, playerId uint64) {
	proxy := New(conn, laddr, s.addr, playerId)

	proxy.Start(firstPacket)

	s.mutex.Lock()
	s.proxies = append(s.proxies, proxy)
	s.mutex.Unlock()

	go proxy.closeConnection()
}

func (s *SubServer) GetLogs(tail string) (string, error) {
	logs, err := manager.executor.ContainerLogs(s.container.ID, s.container.Logs.LastTimestamp, tail)
	if err != nil {
		return "", err
	}
	s.container.Logs.LastTimestamp = strconv.FormatInt(time.Now().Unix(), 10)

	return logs, nil
}
