package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net"
	"os"

	"gitlab.com/brickhill/servermanager/manager/protobuf/hostservice"
	mpb "gitlab.com/brickhill/servermanager/manager/protobuf/manager"
	"google.golang.org/grpc"
)

type server struct {
	mpb.UnimplementedManagerServer
}

func startNHListener() {
	// Listen for incoming connections.
	l, err := net.Listen("tcp", "0.0.0.0:42479")
	if err != nil {
		log.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	// Close the listener when the application closes.
	defer l.Close()
	s := grpc.NewServer()
	mpb.RegisterManagerServer(s, &server{})
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func (s *server) ServerStarted(ctx context.Context, p *mpb.Server) (*mpb.Boolean, error) {
	subServer := manager.findSubServer(p.GetNetId())
	if subServer == nil {
		return nil, errors.New("failed to find subserver")
	}

	if subServer.container.validator != p.GetValidator() {
		return nil, errors.New("invalid validation token sent")
	}

	subServer.startedChan <- true

	manager.executor.ContainerUpdateCPU(subServer.container.ID, 10)

	return &mpb.Boolean{Valid: true}, nil
}

func (s *server) RetrieveNotifications(ctx context.Context, p *mpb.Server) (*mpb.RetrieveNHNotificationsResponse, error) {
	subServer := manager.findSubServer(p.GetNetId())
	if subServer == nil {
		return nil, errors.New("failed to find subserver")
	}

	if subServer.container.validator != p.GetValidator() {
		return nil, errors.New("invalid validation token sent")
	}

	server := manager.findSubServerServer(p.GetNetId())
	if subServer == nil {
		return nil, errors.New("failed to find server")
	}

	var notifs []*mpb.NHNotification

notiLoop:
	for k, noti := range server.notifications {
		// ignore notifications sent before server started
		if subServer.startupTimestamp > noti.sentAt {
			continue
		}

		for _, netId := range noti.readBy {
			if netId == p.GetNetId() {
				continue notiLoop
			}
		}
		server.notifications[k].readBy = append(noti.readBy, p.GetNetId())

		notifs = append(notifs, &mpb.NHNotification{
			MessageType: uint64(noti.MsgType),
			Body:        noti.Body,
		})
	}

	return &mpb.RetrieveNHNotificationsResponse{Notifications: notifs}, nil
}

func (s *server) HTTPRequest(ctx context.Context, p *mpb.HTTP) (*mpb.HTTPResponse, error) {
	server := manager.findSubServerServer(p.GetNetId())
	if server == nil {
		return nil, errors.New("failed to find server")
	}

	if server.subServers[p.GetNetId()].container.validator != p.GetValidator() {
		return nil, errors.New("invalid validation token sent")
	}

	if p.GetProtocol() != "GET" && p.GetProtocol() != "POST" {
		return nil, errors.New("only GET and POST supported for protocol")
	}

	client, conn, err := connectToHostService()
	if err != nil {
		log.Println("gRPC connect error", err)
		return nil, err
	}
	defer conn.Close()
	url := p.GetUrl()
	if p.GetAddHostKey() {
		url = fmt.Sprintf("%s&host_key=%s", url, server.hostKey)
	}
	resp, err := client.ServerHTTPReq(context.Background(), &hostservice.ServerHTTPRequest{
		SetId:           server.setID,
		HostKey:         server.hostKey,
		Url:             url,
		Protocol:        p.GetProtocol(),
		FollowRedirects: p.GetFollowRedirects(),
	})
	if err != nil {
		log.Println("gRPC message error", err)
		return nil, err
	}

	// map the headers to something grpc can understand
	var grpcHeaders []*mpb.Header
	for _, header := range resp.GetHeaders() {
		grpcHeaders = append(grpcHeaders, &mpb.Header{
			Key:   header.GetKey(),
			Value: header.GetValue(),
		})
	}

	return &mpb.HTTPResponse{Body: resp.GetBody(), Headers: grpcHeaders}, nil
}

func (s *server) AddPlayer(ctx context.Context, p *mpb.Player) (*mpb.PlayerIP, error) {
	subServer := manager.findSubServer(p.GetNetId())
	if subServer == nil {
		return nil, errors.New("failed to find subserver")
	}

	if subServer.container.validator != p.GetValidator() {
		return nil, errors.New("invalid validation token sent")
	}

	//ip := subServer.proxyQueue[p.GetUserId()]

	subServer.mutex.Lock()
	subServer.players[p.GetUserId()] = p.GetToken()
	subServer.mutex.Unlock()

	manager.executor.ContainerUpdateCPU(subServer.container.ID, len(subServer.players)+9)

	return &mpb.PlayerIP{Ip: "" /*ip.remoteIP*/}, nil
}

func (s *server) RemovePlayer(ctx context.Context, p *mpb.Player) (*mpb.Boolean, error) {
	subServer := manager.findSubServer(p.GetNetId())
	if subServer == nil {
		return nil, errors.New("failed to find subserver")
	}

	if subServer.container.validator != p.GetValidator() {
		return nil, errors.New("invalid validation token sent")
	}

	if _, ok := subServer.players[p.GetUserId()]; ok {
		subServer.mutex.Lock()
		delete(subServer.players, p.GetUserId())
		subServer.mutex.Unlock()
	}

	manager.executor.ContainerUpdateCPU(subServer.container.ID, len(subServer.players)+9)

	return &mpb.Boolean{Valid: true}, nil
}

func (s *server) SetSaveData(ctx context.Context, p *mpb.SetPlayerSaveData) (*mpb.SetPlayerSaveDataResponse, error) {
	if len(p.GetSaveData()) >= 2048 {
		return nil, errors.New("save_data has size limit of 2048 characters")
	}

	server := manager.findSubServerServer(p.GetNetId())
	if server == nil {
		return nil, errors.New("failed to find server")
	}

	if server.subServers[p.GetNetId()].container.validator != p.GetValidator() {
		return nil, errors.New("invalid validation token sent")
	}

	client, conn, err := connectToHostService()
	if err != nil {
		log.Println("gRPC connect error", err)
		return nil, err
	}
	defer conn.Close()
	_, err = client.PlayerSavingData(context.Background(), &hostservice.PlayerSavingDataRequest{SetId: server.setID, HostKey: server.hostKey, PlayerId: p.GetPlayerId(), SaveData: p.GetSaveData()})
	if err != nil {
		log.Println("gRPC message error", err)
		return nil, err
	}

	return &mpb.SetPlayerSaveDataResponse{Saved: true}, nil
}

func (s *server) GetSaveData(ctx context.Context, p *mpb.GetPlayerSaveData) (*mpb.RetrievePlayerSaveDataResponse, error) {
	server := manager.findSubServerServer(p.GetNetId())
	if server == nil {
		return nil, errors.New("failed to find server")
	}

	if server.subServers[p.GetNetId()].container.validator != p.GetValidator() {
		return nil, errors.New("invalid validation token sent")
	}

	client, conn, err := connectToHostService()
	if err != nil {
		log.Println("gRPC connect error", err)
		return nil, err
	}
	defer conn.Close()
	msg, err := client.PlayerRetrievingData(context.Background(), &hostservice.PlayerRetrievingDataRequest{SetId: server.setID, HostKey: server.hostKey, PlayerId: p.GetPlayerId()})
	if err != nil {
		log.Println("gRPC message error", err)
		return nil, err
	}
	return &mpb.RetrievePlayerSaveDataResponse{SaveData: msg.GetSaveData()}, nil
}
