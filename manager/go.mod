module gitlab.com/brickhill/servermanager/manager

go 1.16

require (
	github.com/Microsoft/go-winio v0.5.0 // indirect
	github.com/containerd/containerd v1.5.5 // indirect
	github.com/docker/docker v20.10.8+incompatible
	github.com/docker/go-connections v0.4.0
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/moby/term v0.0.0-20210619224110-3f7ff695adc6 // indirect
	github.com/morikuni/aec v1.0.0 // indirect
	gitlab.com/NebulousLabs/go-upnp v0.0.0-20210414172302-67b91c9a5c03
	golang.org/x/time v0.0.0-20210723032227-1f47c861a9ac // indirect
	google.golang.org/grpc v1.39.1
	google.golang.org/protobuf v1.27.1
)
