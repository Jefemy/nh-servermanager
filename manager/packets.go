package main

import (
	"bytes"
	"compress/zlib"
	"errors"
	"io/ioutil"
	"strings"
)

/**
 * Code from https://github.com/penguib/Go-Hill to decode Node-Hill packets to intercept the auth token to retrieve set attempting to be connected to
 */

type Data struct {
	MessageSize uint
	End         uint8
}

func readUIntV(buffer *[]byte) (*Data, error) {
	reader := new(bytes.Buffer)

	if len(*buffer) == 0 {
		return nil, errors.New("uintv buffer is empty")
	}

	if ((*buffer)[0] & 1) != 0 {
		return &Data{
			MessageSize: uint((*buffer)[0] >> 1),
			End:         1,
		}, nil
	} else if ((*buffer)[0] & 2) != 0 {
		b := make([]byte, 2)
		uint16LE, err := reader.Read(b)
		if err != nil {
			return nil, err
		}
		return &Data{
			MessageSize: uint((uint16LE >> 2) + 0x80),
			End:         2,
		}, nil
	} else if ((*buffer)[0] & 4) != 0 {
		size := (uint16((*buffer)[2]) << 13) + uint16(((*buffer)[1]<<5)+((*buffer)[0]>>3)) + 0x4080
		return &Data{
			MessageSize: uint(size),
			End:         3,
		}, nil
	} else {
		b := make([]byte, 4)
		uint32LE, err := reader.Read(b)
		if err != nil {
			return nil, err
		}
		size := (uint32LE / 8) + 0x204080
		return &Data{
			MessageSize: uint(size),
			End:         3,
		}, nil
	}
}

func getAuthToken(buff []byte) (token string, err error) {
	bu, err := readUIntV(&buff)
	if err != nil {
		return
	}

	packet := buff[bu.End:]

	parsedPacket := packet[:bu.MessageSize]
	r := bytes.NewReader(parsedPacket)
	var packetType uint8
	var buffer *bytes.Buffer

	z, err := zlib.NewReader(r)
	if err != nil {
		buffer = bytes.NewBuffer(parsedPacket)
		packetType, _ = buffer.ReadByte()
		err = nil
	} else {
		p, _ := ioutil.ReadAll(z)
		buffer = bytes.NewBuffer(p)
		packetType, _ = buffer.ReadByte()
	}

	if packetType == 1 {
		token, err = buffer.ReadString(0)
		token = strings.TrimSuffix(token, "\x00")
	} else {
		err = errors.New("auth packet was not first packet sent")
	}

	return
}
