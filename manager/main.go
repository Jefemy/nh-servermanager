package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/joho/godotenv"
	"gitlab.com/brickhill/servermanager/manager/protobuf/hostservice"
)

var (
	netid    = uint64(0)
	hostPort = uint16(42480)

	manager = Manager{}

	tmpDir string
)

func init() {
	err := godotenv.Load(".env")
	if err != nil {
		panic("Error loading .env")
	}

	tmpDir, err = ioutil.TempDir("", "setdata")
	if err != nil {
		panic("error creating temp")
	}
}

func main() {
	manager.executor = NewExecutor()

	go startNHListener()
	go manager.executor.RemoveManagedContainers()

	laddr, _ := net.ResolveTCPAddr("tcp", fmt.Sprintf(":%d", hostPort))
	listener, err := net.ListenTCP("tcp", laddr)
	if err != nil {
		log.Printf("Failed to open local port to listen: %s", err)
		os.Exit(1)
	}

	if os.Getenv("DONT_UPNP") != "true" {
		forwardPort(hostPort)
	}

	manager.laddr = laddr
	manager.listener = listener

	if os.Getenv("INIT_ALL_SERVERS") == "true" {
		client, conn, err := connectToHostService()
		if err != nil {
			panic(err)
		}
		defer conn.Close()
		data, err := client.RetrieveAllServers(context.Background(), &hostservice.RetrieveAllServersRequest{})
		if err != nil {
			panic(err)
		}

		for _, v := range data.GetServerData() {
			server := &Server{
				setID:      v.GetSetId(),
				hostKey:    v.GetHostKey(),
				subServers: make(map[uint64]*SubServer),
			}

			err = server.retrieveFiles()
			if err != nil {
				panic(err)
			}

			manager.availableServers = append(manager.availableServers, server)
		}
	}

	go manager.Listen()
	go manager.cleanServers()
	go manager.cleanContainers()
	go manager.watchNotifications()

	if os.Getenv("DONT_POST") != "true" {
		go func() {
			for {
				go manager.postServers()
				if len(os.Getenv("IS_OFFICIAL")) > 0 {
					go manager.registerManager()
				}
				<-time.After(60 * time.Second)
			}
		}()
	}

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGTERM)
	<-shutdown

	// prevent new connections from forming
	manager.listener.Close()
	manager.inShutdown = true

	// send all servers a shutdown request
	// Node-Hill rechecks notifications at 10 second intervals, and gives a 10 second countdown to shutdown
	for _, server := range manager.availableServers {
		server.notifications = append(server.notifications, ServerNotification{
			MsgType: 1,
			sentAt:  time.Now().Unix(),
		})
	}

	// containers will now enter shutdown
	// after all users have been kicked from the server they will have 30 seconds to cleanly exit before Docker sends SIGKILL

	for {
		<-time.After(5 * time.Second)
		containers, err := manager.executor.ListManagedContainers()
		if err != nil {
			log.Println("error listing containers", err)
			continue
		}

		// all Node-Hill containers have successfully shutdown
		// manager can now exit cleanly
		if len(containers) == 0 {
			// remove tmp set files
			os.RemoveAll(tmpDir)
			break
		}
	}
}
