package main

import (
	"os"
	"strconv"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type AWS struct {
	session *session.Session
	dynamo  *dynamodb.DynamoDB
	sqs     *sqs.SQS
}

type DynamoRow struct {
	UserID uint64                 `json:"user_id"`
	SetID  uint64                 `json:"set_id"`
	Data   interface{}            `json:"data"`
}

func (a *AWS) SaveInDatastore(input DynamoRow) error {
	dynamoMap, err := dynamodbattribute.MarshalMap(input)
	if err != nil {
		return err
	}

	params := &dynamodb.PutItemInput{
		Item:      dynamoMap,
		TableName: aws.String(os.Getenv("DYNAMO_TABLE")),
	}
	_, err = a.dynamo.PutItem(params)
	if err != nil {
		return err
	}

	return nil
}

func (a *AWS) RetrieveFromDatastore(userID uint64, setID uint64) (*DynamoRow, error) {
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"user_id": {
				N: aws.String(strconv.Itoa(int(userID))),
			},
			"set_id": {
				N: aws.String(strconv.Itoa(int(setID))),
			},
		},
		TableName: aws.String(os.Getenv("DYNAMO_TABLE")),
	}

	result, err := a.dynamo.GetItem(input)
	if err != nil {
		return nil, err
	}

	var output DynamoRow

	err = dynamodbattribute.UnmarshalMap(result.Item, &output)
	if err != nil {
		return nil, err
	}

	return &output, nil
}
