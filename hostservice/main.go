package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"net"
	"net/url"
	"os"
	"regexp"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/joho/godotenv"
	"go.uber.org/ratelimit"
	"google.golang.org/grpc"

	pb "gitlab.com/brickhill/servermanager/hostservice/protobuf"
)

type server struct {
	pb.UnimplementedHostServiceServer
}

var (
	awsManager = AWS{}

	buckets = map[uint64]ratelimit.Limiter{}

	optionalHostKeys = map[int]SetData{
		1: {hostKey: "QdaJxP3uuYSGhQOKaNvDlsIbXm9QYfMIAWeTQ82PHclSm0N3NBfrCWlA9wAgy8t6", maxPlayers: 5},
	}

	sqsNotifications = map[int][]Notification{}
)

func init() {
	if os.Getenv("DONT_USE_ENV") != "true" {
		err := godotenv.Load(".env")
		if err != nil {
			panic("Error loading .env")
		}
	}

	awsManager.session, _ = session.NewSession(&aws.Config{Region: aws.String("us-east-1")})
	awsManager.dynamo = dynamodb.New(awsManager.session)
	awsManager.sqs = sqs.New(awsManager.session)
}

func main() {
	if os.Getenv("USE_SQS") == "true" {
		go awsManager.PollSQS()
	}

	// Listen for incoming connections.
	l, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%s", os.Getenv("HOSTSERVICE_PORT")))
	if err != nil {
		log.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	// Close the listener when the application closes.
	defer l.Close()
	s := grpc.NewServer()
	pb.RegisterHostServiceServer(s, &server{})
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func getSetBucket(setID uint64) ratelimit.Limiter {
	if lim, ok := buckets[setID]; ok {
		return lim
	}

	// TODO: this needs to be based off how many players are ingame not a random value
	buckets[setID] = ratelimit.New(15, ratelimit.WithSlack(15))

	return buckets[setID]
}

func (s *server) ServerCanBeHosted(ctx context.Context, m *pb.ServerCanBeHostedRequest) (*pb.ServerCanBeHostedResponse, error) {
	set, err := GetSetById(int(m.GetSetId()), true)
	if err != nil {
		return nil, err
	}

	return &pb.ServerCanBeHostedResponse{Can: true, HostKey: set.hostKey, MaxPlayers: uint64(set.maxPlayers)}, nil
}

func (s *server) RetrieveNotifications(ctx context.Context, m *pb.RetrieveNotificationsRequest) (*pb.RetrieveNotificationsResponse, error) {
	if os.Getenv("USE_SQS") != "true" {
		return nil, errors.New("HostService must be using SQS to retrieve notifications")
	}

	var data []*pb.Notification

	for _, val := range m.GetServerData() {
		bucket := getSetBucket(val.GetSetId())
		bucket.Take()

		set, err := GetSetById(int(val.GetSetId()), false)
		if err != nil {
			return nil, err
		}
		if set.hostKey != val.GetHostKey() {
			return nil, errors.New("host key does not match")
		}

		notifs := sqsNotifications[int(val.GetSetId())]
		for _, noti := range notifs {
			data = append(data, &pb.Notification{
				SetId:       val.GetSetId(),
				MessageType: uint64(noti.Type),
				Body:        noti.Body,
			})
		}
		sqsNotifications[int(val.GetSetId())] = []Notification{}
	}

	return &pb.RetrieveNotificationsResponse{Notification: data}, nil
}

func (s *server) RetrieveAllServers(ctx context.Context, m *pb.RetrieveAllServersRequest) (*pb.RetrieveAllServersResponse, error) {
	if os.Getenv("SHOULD_USE_DATABASE") == "true" {
		return nil, errors.New("HostService must be using local variable to list all servers")
	}

	var data []*pb.ServerData

	for k, val := range optionalHostKeys {
		data = append(data, &pb.ServerData{
			SetId:      uint64(k),
			HostKey:    val.hostKey,
			MaxPlayers: uint64(val.maxPlayers),
		})
	}

	return &pb.RetrieveAllServersResponse{ServerData: data}, nil
}

func (s *server) RetrieveServerData(ctx context.Context, m *pb.RetrieveServerDataRequest) (*pb.RetrieveServerDataResponse, error) {
	// TODO: seems weird to have an external service send over file names, look into better way to get this
	return &pb.RetrieveServerDataResponse{BundleUrl: fmt.Sprintf("https://api.brick-hill.com/v1/games/getBrk?host_key=%s", m.GetHostKey())}, nil
}

func (s *server) PlayerAttemptingToJoin(ctx context.Context, p *pb.PlayerAttemptingToJoinRequest) (*pb.PlayerAttemptingToJoinResponse, error) {
	// TODO: this regex doesnt seem to work, it appears to return true when passed 'undefined' - need to look into more
	matches, _ := regexp.MatchString(`[\w]{8}(-[\w]{4}){3}-[\w]{12}`, p.GetAuthToken())
	// if !matches token is either invalid or local
	if matches {
		body, _, err := httpRequest("GET", fmt.Sprintf("https://api.brick-hill.com/v1/auth/tokenData?token=%s", url.QueryEscape(p.GetAuthToken())), true)
		if err != nil {
			return nil, errors.New("error retrieving set data")
		}

		type tokenRequest struct {
			SetID  uint64 `json:"set_id"`
			UserID uint64 `json:"user_id"`
		}
		var jsonData tokenRequest

		err = json.Unmarshal(body, &jsonData)
		if err != nil {
			return nil, errors.New("invalid json retrieved")
		}

		return &pb.PlayerAttemptingToJoinResponse{SetId: jsonData.SetID, PlayerId: jsonData.UserID}, nil
	}

	if os.Getenv("SHOULD_ALLOW_LOCAL") != "true" {
		return nil, errors.New("invalid authentication token")
	}

	// manager wont need to know real userid for local servers so just return random
	return &pb.PlayerAttemptingToJoinResponse{SetId: 1, PlayerId: uint64(rand.Intn(25000))}, nil
}

func (s *server) PlayerSavingData(ctx context.Context, p *pb.PlayerSavingDataRequest) (*pb.PlayerSavingDataResponse, error) {
	if os.Getenv("SHOULD_USE_DYNAMO") != "true" {
		return nil, errors.New("dynamo datastore not enabled")
	}

	if len(p.GetSaveData()) > 2048 {
		return nil, errors.New("save_data has size limit of 2048 characters")
	}

	bucket := getSetBucket(p.GetSetId())
	bucket.Take()

	set, err := GetSetById(int(p.GetSetId()), false)
	if err != nil {
		return nil, err
	}
	if set.hostKey != p.GetHostKey() {
		return nil, errors.New("host key does not match")
	}

	var dataMap interface{}

	json.Unmarshal([]byte(p.GetSaveData()), &dataMap)
	if err := awsManager.SaveInDatastore(DynamoRow{
		UserID: p.GetPlayerId(),
		SetID:  p.GetSetId(),
		Data:   dataMap,
	}); err != nil {
		return &pb.PlayerSavingDataResponse{Saved: false}, err
	}

	return &pb.PlayerSavingDataResponse{Saved: true}, nil
}

func (s *server) PlayerRetrievingData(ctx context.Context, p *pb.PlayerRetrievingDataRequest) (*pb.PlayerRetrievingDataResponse, error) {
	if os.Getenv("SHOULD_USE_DYNAMO") != "true" {
		return nil, errors.New("dynamo datastore not enabled")
	}

	bucket := getSetBucket(p.GetSetId())
	bucket.Take()

	set, err := GetSetById(int(p.GetSetId()), false)
	if err != nil {
		return nil, err
	}
	if set.hostKey != p.GetHostKey() {
		return nil, errors.New("host key does not match")
	}

	out, err := awsManager.RetrieveFromDatastore(p.GetPlayerId(), p.GetSetId())
	if err != nil {
		return nil, err
	}

	data, err := json.Marshal(out.Data)

	return &pb.PlayerRetrievingDataResponse{SaveData: string(data)}, err
}

func (s *server) ServerHTTPReq(ctx context.Context, p *pb.ServerHTTPRequest) (*pb.ServerHTTPResponse, error) {
	bucket := getSetBucket(p.GetSetId())
	bucket.Take()

	set, err := GetSetById(int(p.GetSetId()), false)
	if err != nil {
		return nil, err
	}
	if set.hostKey != p.GetHostKey() {
		return nil, errors.New("host key does not match")
	}

	// TODO: swap domain to internal domain to keep data from leaving to public networks?
	if !strings.HasPrefix(p.GetUrl(), "https://api.brick-hill.com/") {
		return nil, errors.New("requests must be on https://api.brick-hill.com domain")
	}
	body, headers, err := httpRequest(p.GetProtocol(), p.GetUrl(), p.GetFollowRedirects())
	if err != nil {
		return nil, err
	}

	// map the headers to something grpc can understand
	var grpcHeaders []*pb.ServerHeader
	for key, value := range headers {
		grpcHeaders = append(grpcHeaders, &pb.ServerHeader{
			Key:   key,
			Value: value,
		})
	}

	return &pb.ServerHTTPResponse{Body: body, Headers: grpcHeaders}, nil
}
