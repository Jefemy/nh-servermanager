module gitlab.com/brickhill/servermanager/hostservice

go 1.16

require (
	github.com/aws/aws-sdk-go v1.40.16
	github.com/go-sql-driver/mysql v1.6.0
	github.com/joho/godotenv v1.3.0
	go.uber.org/ratelimit v0.2.0
	google.golang.org/grpc v1.39.1
	google.golang.org/protobuf v1.27.1
)
