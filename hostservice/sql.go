package main

import (
	"database/sql"
	"errors"
	"fmt"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var (
	db *sql.DB

	setCache = make(map[int]SetData)
)

type SetData struct {
	hostKey    string
	maxPlayers int
}

func init() {
	if os.Getenv("SHOULD_USE_DATABASE") != "true" {
		return
	}

	dbConn := fmt.Sprintf("%s:%s@tcp(%s)/%s", os.Getenv("DB_USERNAME"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_HOST"), os.Getenv("DB_DATABASE"))
	database, err := sql.Open("mysql", dbConn)
	if err != nil {
		panic(err.Error())
	}
	database.SetConnMaxLifetime(1 * time.Minute)

	db = database
}

func CloseDB() {
	db.Close()
}

func GetSetById(setID int, shouldRefreshCache bool) (setData SetData, err error) {
	if os.Getenv("SHOULD_USE_DATABASE") != "true" {
		if val, ok := optionalHostKeys[setID]; ok {
			return val, nil
		}

		return SetData{}, errors.New("host key could not be found")
	}

	if !shouldRefreshCache {
		if val, ok := setCache[setID]; ok {
			return val, nil
		}
	}

	var rawSetData struct {
		hostKey     sql.NullString
		maxPlayers  sql.NullInt64
		isDedicated sql.NullInt64
	}

	err = db.QueryRow("SELECT `host_key`, `max_players`, `is_dedicated` FROM `sets` WHERE `id` = ?", setID).Scan(&rawSetData.hostKey, &rawSetData.maxPlayers, &rawSetData.isDedicated)

	if err != nil {
		return SetData{}, err
	}

	if rawSetData.hostKey.Valid {
		setData.hostKey = rawSetData.hostKey.String
	} else {
		return SetData{}, errors.New("set has null host_key")
	}

	if rawSetData.maxPlayers.Valid {
		setData.maxPlayers = int(rawSetData.maxPlayers.Int64)
	} else {
		setData.maxPlayers = 12
	}

	setCache[int(setID)] = setData

	if !rawSetData.isDedicated.Valid || rawSetData.isDedicated.Int64 != 1 {
		return SetData{}, errors.New("set is not setup for dedicated hosting")
	}

	return
}
