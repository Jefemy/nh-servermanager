package main

import (
	"encoding/json"
	"os"
	"strconv"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type Notification struct {
	Type int    `json:"type"`
	Body string `json:"body"`
}

func (a *AWS) PollSQS() {
	for {
		output, err := a.sqs.ReceiveMessage(&sqs.ReceiveMessageInput{
			QueueUrl:            aws.String(os.Getenv("SQS_URL")),
			MaxNumberOfMessages: aws.Int64(10),
			WaitTimeSeconds:     aws.Int64(20),
			MessageAttributeNames: []*string{
				aws.String(sqs.QueueAttributeNameAll),
			},
		})

		if err != nil {
			continue
		}

		for _, message := range output.Messages {
			setID, err := strconv.Atoi(*message.MessageAttributes["SetId"].StringValue)
			if err != nil {
				continue
			}

			var jsonData Notification
			err = json.Unmarshal([]byte(*message.Body), &jsonData)
			if err != nil {
				continue
			}

			// dont want notifications stacking up too much
			if len(sqsNotifications[setID]) > 10 {
				continue
			}

			sqsNotifications[setID] = append(sqsNotifications[setID], Notification{
				jsonData.Type,
				jsonData.Body,
			})

			a.sqs.DeleteMessage(&sqs.DeleteMessageInput{
				QueueUrl:      aws.String(os.Getenv("SQS_URL")),
				ReceiptHandle: message.ReceiptHandle,
			})
		}
	}
}
