package main

import (
	"io/ioutil"
	"net/http"

	"go.uber.org/ratelimit"
)

var (
	// the hostservice processes under one IP address, BH has a rate limit that will error out requests if it happens to be processing them too fast
	// could technically be a bit higher but dont really want to host a brick hill dos machine on production servers
	httpLimiter = ratelimit.New(15)
)

func httpRequest(proto, url string, followRedirects bool) (body []byte, headers map[string]string, err error) {
	httpLimiter.Take()

	httpClient := &http.Client{}

	if !followRedirects {
		httpClient.CheckRedirect = func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		}
	}

	req, err := http.NewRequest(proto, url, nil)
	if err != nil {
		return
	}

	resp, err := httpClient.Do(req)
	if err != nil {
		return
	}

	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	// http headers returned are an array of strings since technically the request can send multiple of the same headers
	// nothing we use ever returns that and its not really a compatibility we need to support so just map it down to a single value per header
	headers = make(map[string]string)
	for key := range resp.Header {
		headers[key] = resp.Header.Get(key)
	}

	return
}
