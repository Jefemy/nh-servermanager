#!/bin/sh

export DOCKER_HOST_IP=$(route -n | awk '/UG[ \t]/{print $2}')
NODE_ENV=production exec node start.js