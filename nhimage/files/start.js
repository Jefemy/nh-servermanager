const nh = require("node-hill");
const { debundleMapData } = require("nh-bundle/lib");

const Game = require("node-hill/dist/src/class/Game").default;
const Sanction = require("node-hill/dist/src/class/Sanction").default;

async function init() {
    debundleMapData({
        bundlePath: "./set.bbrk",
        map: "./maps/",
        scripts: {
            directory: "./user_scripts/",
        },
    });

    Game.shutdown = function () {
        process.kill(process.pid, "SIGTERM");
    };

    Game.MOTD = `[#14d8ff][NOTICE]: This server is hosted by Brick Hill using node-hill ${Game.version}.`;

    // TODO: sanctions are harder to implement than expected -- changing the ip in grpc.js doesnt replicate over to the socket and keeps the old ip
    // TODO: another realization is that they will have to authenticate before i can get their ip slightly ruining the use case of sanctions
    // TODO: maybe i should just implement the node sanctions into the go manager so it stops it before even attempting to proxy?
    // TODO: meanwhile, replace banSocket with empty function so a user cant just ban everyone from a server
    Sanction.banSocket = () => true;

    // I believe all connections will be this IP but should probably verify it
    Sanction.allowedIPs.add("172.20.0.1");

    nh.startServer({
        hostKey: process.env.NH_HOST_KEY,
        gameId: process.env.NH_SET,
        local: process.env.NH_LOCAL == "true",
        ip: "0.0.0.0",
        port: 42480,
        postServer: false,
        mapDirectory: "./maps/",
        map: "bundled.brk",
        scripts: "./user_scripts",
        modules: ["/home/nhuser/nodehill/custom/grpc.js"],
        cli: false,
    }).then((Game) => {
        Game.gameId = process.env.NH_SET;
        Game.getSetData(Game.gameId)
            .then((data) => {
                Game.setData = data;
                Game.emit("setDataLoaded");
            })
            .catch(() => {});
    });
}

init();
