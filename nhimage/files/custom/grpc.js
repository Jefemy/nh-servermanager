const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');

class GRPCManager {
    constructor(game) {
        this.game = game

        const PROTO_PATH = __dirname + '/protobuf/manager.proto';
        const packageDefinition = protoLoader.loadSync(
            PROTO_PATH,
            {
                keepCase: true,
                longs: String,
                enums: String,
                defaults: true,
                oneofs: true
            }
        );
        let protoDescriptor = grpc.loadPackageDefinition(packageDefinition);
        let main = protoDescriptor.main;

        this.manager = new main.Manager(`${process.env.DOCKER_HOST_IP}:42479`, grpc.credentials.createInsecure());
    }

    listenPlayers() {
        // TODO: hide validationToken from NH, manager handles all server posts and is the only one that needs access to the token
        this.game.on("playerJoin", (p) => {
            this.manager.AddPlayer({
                net_id: process.env.NET_ID,
                validator: process.env.NH_VALIDATOR,
                user_id: p.userId,
                token: p.validationToken
            }, (err, m) => {
                //p.socket.IPV4 = m.ip;
                if(err) return p.kick("Error in joining");
            });
        })
        
        this.game.on("playerLeave", (p) => {
            this.manager.RemovePlayer({
                net_id: process.env.NET_ID,
                validator: process.env.NH_VALIDATOR,
                user_id: p.userId,
                token: p.validationToken
            }, (err) => {
                // what should it do here?
                if(err) return;
            });
        })
    }

    receiveNotifications() {
        this.manager.RetrieveNotifications({
            net_id: process.env.NET_ID,
            validator: process.env.NH_VALIDATOR
        }, (err, m) => {
            if(err) return;

            for(let notif of m.notifications) {
                switch(notif.message_type) {
                    case "1":
                        (async () => {
                            for(let i = 0; i < 10; i++) {
                                this.game.centerPrintAll(`[#f23d00]Game will restart in ${10 - i} seconds`);
                                await new Promise(r => setTimeout(r, 1000));
                            }
                            this.game.shutdown();
                        })()
                        break;
                }
            }
        })
    }

    doHttp(protocol, url, opts = {}) {
        return new Promise((resolve, reject) => this.manager.HTTPRequest({
            net_id: process.env.NET_ID,
            validator: process.env.NH_VALIDATOR,
            url: url,
            add_host_key: opts.addHostKey ?? false,
            protocol: protocol,
            follow_redirects: opts.followRedirects ?? true
        }, (err, m) => {
            if(err) return reject(JSON.stringify(err));

            let data = m.body
            if((opts.isJSON || false)) {
                try {
                    data = JSON.parse(m.body)
                } catch {
                    reject("invalid json")
                }
            }
            let headers = m.headers.reduce((map, header) => {
                map[header.key.toLowerCase()] = header.value;
                return map;
            }, {})
            resolve({
                body: data,
                headers: headers
            });
        }))
    }

    getSaveData(playerId) {
        return new Promise((resolve, reject) => this.manager.GetSaveData({
            net_id: process.env.NET_ID,
            validator: process.env.NH_VALIDATOR,
            player_id: playerId
        }, (err, m) => {
            if(err) return reject(JSON.stringify(err));

            let data
            try {
                data = JSON.parse(m.save_data)
            } catch {
                data = {}
            }
            resolve(data)
        }))
    }

    setSaveData(playerId, data) {
        data = JSON.stringify(data)
        return new Promise((resolve, reject) => this.manager.SetSaveData({
            net_id: process.env.NET_ID,
            validator: process.env.NH_VALIDATOR,
            player_id: playerId,
            save_data: data
        }, (err, m) => {
            if(err) return reject(JSON.stringify(err));
            if(!m.saved) return reject("data failed to save for unknown reason");

            resolve()
        }))
    }
}

module.exports = {
    init: (game) => {
        game.grpc = new GRPCManager(game)
        game.grpc.manager.ServerStarted({
            net_id: process.env.NET_ID,
            validator: process.env.NH_VALIDATOR
        }, (err) => {
            // if the manager doesnt know the server started just shutdown
            if(err) return game.shutdown();
        })

        setInterval(() => game.grpc.receiveNotifications(), 10 * 1000)
    },
    manager: () => new GRPCManager()
}