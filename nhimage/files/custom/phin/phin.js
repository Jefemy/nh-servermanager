const { URL, URLSearchParams } = require('url')

const grpc = require('../grpc')

/**
* Sends an HTTP request
* @param {phinOptions|string} options - phin options object (or string for auto-detection)
* @returns {Promise<http.serverResponse>} - phin-adapted response object
*/
const phin = async (opts) => {
	if (typeof(opts) !== 'string') {
		if (!opts.hasOwnProperty('url')) {
			throw new Error('Missing url option from options for request method.')
		}
	}

    if((opts.method || 'GET') !== 'GET') {
        throw new Error('Only GET supported');
    }

    let manager = grpc.manager();

    const parsedUrl = new URL(typeof opts === 'object' ? opts.url : opts);

    if(parsedUrl.hostname !== 'api.brick-hill.com') {
        throw new Error('Only api.brick-hill.com requests supported')
    }

    const search = new URLSearchParams(parsedUrl.searchParams);

    let hasHostKey = false;

    if(search.has('host_key')) {
        hasHostKey = true;
        search.delete('host_key');

        parsedUrl.search = search;
    }

    const data = await manager.doHttp("GET", parsedUrl, {
        isJSON: (opts.parse == "json"),
        addHostKey: hasHostKey,
        followRedirects: opts.followRedirects ?? false
    })

    return {
        body: data.body,
        headers: data.headers,
        statusCode: (typeof data.headers.location !== 'undefined') ? 302 : 200
    };
}

// If we're running Node.js 8+, let's promisify it

phin.promisified = phin

phin.unpromisified = (opts, cb) => {
	phin(opts).then((data) => {
		if (cb) cb(null, data)
	}).catch((err) => {
		if (cb) cb(err, null)
	})
}

// Defaults

phin.defaults = (defaultOpts) => async (opts) => {
	const nops = typeof opts === 'string' ? {'url': opts} : opts

	Object.keys(defaultOpts).forEach((doK) => {
		if (!nops.hasOwnProperty(doK) || nops[doK] === null) {
			nops[doK] = defaultOpts[doK]
		}
	})

	return await phin(nops)
}

module.exports = phin