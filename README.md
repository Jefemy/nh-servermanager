# nh-servermanager

Go manager for [Node-Hill](https://gitlab.com/brickhill/open-source/node-hill) that acts as a proxy to connect to it for use in managing multiple Node-Hill instances.

## Features

- Uses [Docker](https://www.docker.com/) to make Node-Hill user script escapes less dangerous
- Launches multiple Node-Hill instances for the same set, making it possible to split a sets players across servers to reduce load on the client
- Automatically starts and closes new Node-Hill instances to keep the minimum amount of servers running to reduce load on the server

## Requirements

- Docker must be installed on the host server
- HostService:
  - Datastore requires DynamoDB
  - Access to database is optional, there is a `optionalHostKeys` variable that allows you to add host keys in the format of `set_id: host_key`
    - Alternatively you can host your own mysql database that has a `sets` table with `id` and `host_key` to allow the service to search for the key
  - Should be hosted on a different server than the manager to prevent any container escapes from ever getting any permissions
    - If you are only ever hosting your own sets this advice can be ignored

## Setup

//
